<?php

$config['imap_delimiter'] = '/';
$config['dont_override'] = array(
	'skip_deleted',
	'flag_for_deletion',
	'reply_mode',
	'delete_junk'
);
$config['cipher_method'] = 'AES-256-CBC';
$config['username_domain'] = 'libc.it';
$config['request_path'] = '.';
$config['undo_timeout'] = 30;
$config['show_images'] = 1;
$config['htmleditor'] = 4;
$config['skip_deleted'] = false;
$config['read_when_deleted'] = true;
$config['flag_for_deletion'] = false;
$config['check_all_folders'] = true;
$config['reply_mode'] = 1;
$config['delete_junk'] = true;
$config['default_font'] = 'Calibri';
$config['default_font_size'] = '11pt';
$config['session_lifetime'] = 10080;
$config['proxy_whitelist'] = [
	'::1',
    '127.0.0.1',
    '172.17.0.1' # docker host
];
