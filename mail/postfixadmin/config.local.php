<?php
$CONF['configured'] = true;

$CONF['setup_password'] = 'TEMPLATE_MAILSETUP_PASSWORD';

$CONF['database_type'] = 'mysqli';
$CONF['database_host'] = 'mail-mysql';
$CONF['database_user'] = 'mail';
$CONF['database_password'] = 'TEMPLATE_MAIL_PASSWORD';
$CONF['database_name'] = 'mail';

$CONF['admin_email'] = 'root@libc.it';
$CONF['admin_smtp_password'] = 'TEMPLATE_ROOT_MAIL_PASSWORD';

$CONF['smtp_server'] = 'mx.libc.it';
$CONF['smtp_port'] = '587';
$CONF['smtp_sendmail_tls'] = 'YES';

$CONF['default_aliases'] = array (
    'abuse' => 'root@libc.it',
    'hostmaster' => 'root@libc.it',
    'postmaster' => 'root@libc.it',
    'webmaster' => 'root@libc.it'
);

$CONF['aliases'] = '0';
$CONF['mailboxes'] = '0';

$CONF['fetchmail'] = 'NO';

$CONF['footer_text'] = 'Return to Mail';
$CONF['footer_link'] = 'https://mail.libc.it';
