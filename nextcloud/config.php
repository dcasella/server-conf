<?php
$CONFIG = array (
  'instanceid' => 'ocm277p2psdg',
  'passwordsalt' => 'kZxW/Ycc4CCMlC5k+fT1Tk9gnEaSbK',
  'secret' => 'BLoGwfDVxbfwrN1bHXjeM0e7aAf7SLXtbIY3pNTLxm356nDa',
  'trusted_domains' =>
  array (
    0 => 'cloud.libc.it',
    1 => 'libc.it',
  ),
  'trusted_proxies' =>
  array (
    0 => '::1',
    1 => '127.0.0.1',
    2 => '172.17.0.1', # docker host
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'mysql',
  'version' => 'TEMPLATE_NEXTCLOUD_VERSION',
  'overwriteprotocol' => 'https',
  'overwrite.cli.url' => 'https://cloud.libc.it',
  'dbname' => 'nextcloud',
  'dbhost' => 'nextcloud-mysql',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'nextcloud',
  'dbpassword' => 'TEMPLATE_NEXTCLOUD_PASSWORD',
  'installed' => true,
  'default_phone_region' => 'IT',
  'mail_smtpmode' => 'smtp',
  'mail_from_address' => 'noreply',
  'mail_domain' => 'libc.it',
  'mail_smtpauth' => 1,
  'mail_smtpauthtype' => 'PLAIN',
  'mail_smtphost' => 'mx.libc.it',
  'mail_smtpsecure' => 'tls',
  'mail_smtpport' => '587',
  'mail_smtpname' => 'monitor@libc.it',
  'mail_smtppassword' => 'TEMPLATE_MONITOR_MAIL_PASSWORD',
  'mail_sendmailmode' => 'smtp',
  'filelocking.enabled' => true,
  'redis' =>
  array (
    'host' => 'nextcloud-redis',
    'port' => 6379,
  ),
  'memcache.local' => '\OC\Memcache\APCu',
  'memcache.distributed' => '\OC\Memcache\Redis',
  'memcache.locking' => '\OC\Memcache\Redis',
  'user_backends' =>
  array (
    0 =>
    array (
      'class' => '\OCA\UserExternal\IMAP',
      'arguments' =>
      array (
        0 => 'mx.libc.it',
        1 => 993,
        2 => 'ssl',
        3 => 'libc.it', # default domain
        4 => true, # strip domain
        5 => false, # add user to domain group
      ),
    ),
  ),
  'theme' => '',
  'loglevel' => 2,
  'maintenance' => false,
  'updater.release.channel' => 'stable',
  'mysql.utf8mb4' => true,
  'maintenance_window_start' => 4, # 4 AM UTC
);
